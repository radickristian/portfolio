( function( $ ) {
	'use strict';
	$( '.offcanvas-toggle' ).on( 'click', function() {
	  $( 'body' ).toggleClass( 'offcanvas-expanded' );
	} );

	$( '.offcanvas li a' ).on( 'click', function() {
		$( 'body' ).removeClass( 'offcanvas-expanded' );
	} );

	$( '.offcanvas-toggle' ).on( 'click', function() {
	  $( this ).toggleClass( 'active' );
	} );

	$( function() {
		$( '.data-mh' ).matchHeight();
	} );

	$( '.slick' ).slick( {
		fade: true,
		dots: false,
		arrows: true
	 } );
}( jQuery ) );
