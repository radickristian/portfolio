( function() {
	'use strict';

	var getScrollTop = function() {
		return window.pageYOffset ||
			document.documentElement.scrollTop ||
			document.body.scrollTop ||
			0;
	};

	window.getScrollTop = getScrollTop;
}() );
